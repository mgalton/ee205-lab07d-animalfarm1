///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 1 - EE 205 - Spr 2022
///
/// Usage:  addCats
///
/// Result:
///   Adds cats to database
///
/// @file addCats.c
/// @version 1.0
///
/// @author Mariko Galton  <mgalton@hawaii.edu>
/// @date   10_Mar_2022
///////////////////////////////////////////////////////////////////////////////

#include "config.h"
#include "addCats.h" 
#include <stdio.h>
#include <stdbool.h>
#include "string.h"
#include "catDatabase.h"
#include <stdlib.h>


int addCat(const char name[],const int gender,const int breed,const bool isFixed,const float weight, const int collarcolor1, const int collarcolor2, const unsigned long long license) {

   if (strlen(name) >= MAX_CAT_NAME){
      fprintf(stderr, "%s: Name of cat cannot be greater than %d letters\n", "Animalfarm1", MAX_CAT_NAME);
      return 0;
   }

   if (strlen(name) == 0) {
      fprintf(stderr, "Cat name must be entered\n");
      return 0;
   }
  
   
   if (weight <= 0) {
      fprintf(stderr, "Cat weight must be entered\n");
      return 0;
   }

   if ( countcat > MAX_CAT_NUM) {
      fprintf(stderr, "%s: Too many cats. Only %d cats can be in the database at the moment\n", "Animalfarm1", MAX_CAT_NUM);
      return 0;
   
   }

   strncpy( Catstruct_array[countcat].catname, name, MAX_CAT_NAME);
   Catstruct_array[countcat].catgender = gender;
   Catstruct_array[countcat].catbreed = breed;
   Catstruct_array[countcat].catfixed = isFixed;
   Catstruct_array[countcat].catweight = weight;
   Catstruct_array[countcat].collarColor1 = collarcolor1;
   Catstruct_array[countcat].collarColor2 = collarcolor2;
   Catstruct_array[countcat].license = license;

   countcat++;
   
   return 1;
}

