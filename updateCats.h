///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 1 - EE 205 - Spr 2022
///
/// Usage:  updateCats
///
/// Result:
///   Updates cats in database
///
/// @file updateCats.h
/// @version 1.0
///
/// @author Mariko Galton  <mgalton@hawaii.edu>
/// @date   10_Mar_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include <stdio.h>

int updateCatName (const int index, const char* newName);

int fixCat(const int index);

int updateCatWeight(const int index,const float newWeight);

int updateCatCollar1(const int index, const int newcollarColor1);

int updateCatCollar2(const int index, const int newcollarColor2);

int updateCatLicense(const int index, const unsigned long long newLicense);

