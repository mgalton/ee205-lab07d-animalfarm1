////////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
// @brief   Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
//
// @file    config.h
// @version 1.0 - Initial version
//
// @author  Mariko Galton <mgalton@hawaii.edu>
// @date    10_Mar_2022
//
// @see     https://www.gnu.org/software/make/manual/make.html
////////////////////////////////////////////////////////////////////////////////

#define Animalfarm1

