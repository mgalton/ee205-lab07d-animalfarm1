///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 1 - EE 205 - Spr 2022
///
/// Usage:  deleteCats
///
/// Result:
///   Adds cats to database
///
/// @file deleteCats.c
/// @version 1.0
///
/// @author Mariko Galton  <mgalton@hawaii.edu>
/// @date   10_Mar_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "deleteCats.h"
#include "catDatabase.h"
#include "config.h"

int deleteAllCats() {
   countcat = 0;
   return 1;
}
