///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 1 - EE 205 - Spr 2022
///
/// Usage:  addCats
///
/// Result:
///   contains data of the cats
///
/// @file addCats.h
/// @version 1.0
///
/// @author Mariko Galton  <mgalton@hawaii.edu>
/// @date   10_Mar_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once 
#include <stdio.h>
#include <stdbool.h>

int addCat(const char* name, const int gender, const int breed, const bool isFixed, const float weight, const int collarColor1, const int collarColor2, const unsigned long long license);

