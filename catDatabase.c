///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 1 - EE 205 - Spr 2022
///
/// Usage:  catDatabase
///
/// Result:
///   contains data of the cats
///
/// @file catDatabase.c
/// @version 1.0
///
/// @author Mariko Galton  <mgalton@hawaii.edu>
/// @date   10_Mar_2022
///////////////////////////////////////////////////////////////////////////////

#include "catDatabase.h"
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "config.h"

char           catname[MAX_CAT_NAME];
enum           Gender catgender;               // redeclaring Gender as catgender
enum           Breed catbreed;                 // redeclaring Breed as catbreed
enum Color     collarColor1;
enum Color     collarColor2;
unsigned long long   license;
bool           catfixed;                         
float          catweight;

int            countcat = 0;       //Initializing zero as the starting point

struct         Catstruct Catstruct_array[MAX_CAT_NAME];

char* catGender (const enum Gender catgender) {
   switch (catgender) {
      case UNKNOWN_GENDER:
         return "UNKNOWN_GENDER";
      case MALE:
         return "MALE";
      case FEMALE:
         return "FEMALE";
   }
   return 0;
}


char* catBreed (const enum Breed catbreed) {
   switch (catbreed) {
      case UNKOWN_BREED:
         return "UNKNOwN_BREED";
      case MAINE_COON:
         return "MAINE_COON";
      case MANX:
         return "MANX";
      case SHORTHAIR:
         return "SHORTHAIR";
      case PERSIAN:
         return "PERSIAN";
      case SPHYNX:
         return "SPHYNX";
      }
   return 0;
}


char* colorName (const enum Color color) {
   switch (color) {
      case BLACK:
         return "BLACK";
      case WHITE:
         return "WHITE";            
      case RED:
         return "RED";
      case BLUE:
         return "BLUE";
      case GREEN:
         return "GREEN";
      case PINK:
         return "PINK";
      }
      return 0;
}


