///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 1 - EE 205 - Spr 2022
///
/// Usage:  reportCats
///
/// Result:
///   finds cats and prints the database
///
/// @file reportCats.h
/// @version 1.0
///
/// @author Mariko Galton  <mgalton@hawaii.edu>
/// @date   10_Mar_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include <stdio.h>

extern int printCat(const int index);
extern int printAllCats();
extern int findCat(char name[]);

