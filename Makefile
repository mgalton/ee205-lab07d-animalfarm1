###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 05d - Animal Farm 1 - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
###
### @author  Mariko Galton <mgalton@hawaii.edu>
### @date    10_Mar_2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################


CC     = gcc
CFLAGS = -g -Wall -Wextra

TARGET = animalfarm

all: $(TARGET)

test: $(TARGET)
	./$(TARGET)

addCats.o: addCats.c addCats.h catDatabase.h
	$(CC) $(CFLAGS) -c addCats.c

catDatabase.o: catDatabase.c catDatabase.h
	$(CC) $(CFLAGS) -c catDatabase.c

reportCats.o: reportCats.c reportCats.h catDatabase.h
	$(CC) $(CFLAGS) -c reportCats.c

updateCats.o: updateCats.c updateCats.h catDatabase.h
	$(CC) $(CFLAGS) -c updateCats.c

deleteCats.o: deleteCats.c deleteCats.h catDatabase.h
	$(CC) $(CFLAGS) -c deleteCats.c

main.o: main.c addCats.h catDatabase.h reportCats.h updateCats.h deleteCats.h
	$(CC) $(CFLAGS) -c main.c

animalfarm: main.o addCats.o catDatabase.o reportCats.o updateCats.o deleteCats.o
	$(CC) $(CFLAGS) -o $(TARGET) main.o addCats.o catDatabase.o reportCats.o updateCats.o deleteCats.o 


clean:
	rm -f *.o $(TARGET)            
